<?php
abstract class Computer
{
	private $cpu;
	private $ram;
	private $video;
	private $memory;
	private $isWorking = false; //Boolean variable with computer’s current statement (on/off)
	private $computerName = 'Computer';

	public function setCpu($cpu1)
	{
		$this->cpu = $cpu1;
	}
	public function getCpu()
	{
		return $this->cpu;
	}

	public function setRam($ram1)
	{
		$this->ram = $ram1;
	}
	public function getRam()
	{
		return $this->ram;
	}

	public function setVideo($video1)
	{
		$this->video = $video1;
	}
	public function getVideo()
	{
		return $this->video;
	}

	public function setMemory($memory1)
	{
		$this->memory = $memory1;
	}
	public function getMemory()
	{
		return $this->memory;
	}

	public function setComputerName($computerName1)
	{
		$this->computerName = $computerName1;
	}
	public function getComputerName()
	{
		return $this->computerName;
	}

	public function start()
	{
		if($this->isWorking) {
			Console::printLine("start() Computer already Turned On","WARNING");
		} else {
		$this->isWorking = true;
		Console::printLine("start()".$this->getComputerName()." Welcome user!","SUCCESS");
		}
	}

	public function shutdown()
	{
		if ( $this->isWorking == false) {
			Console::printLine("shutdown() Computer Switch Off", "FAILURE");
		} else {
		$this->isWorking = false;
		Console::printLine("shutdown() Goodbuy!", "SUCCESS"); }
	}

	public function restart2()
	{
		if ($this->isWorking) {
			$this->shutDown();
			for ($t = 1; $t <= 5; $t++) {
				echo '.';
				sleep(1);
			}
			echo PHP_EOL;
			$this->start();
		} else {
			Console::printLine("shutdown() Computer must be Turn On for restart", "WARNING");
		}
	}

	public function printParameters()
	{
		if ($this->isWorking) {
			echo $this->computerName . PHP_EOL . $this->cpu . PHP_EOL . $this->ram . PHP_EOL . $this->video . PHP_EOL . $this->memory;
			/*echo $this->cpu;
			echo $this->ram;
			echo $this->video;
			echo $this->memory;*/
		} else {
			echo "Error! Computer OFF. Switch ON computer";
		}
	}

	abstract public function identifyUser();
}





