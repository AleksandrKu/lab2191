<?php
class Lenovo extends Computer
{
	const IS_DESKTOP = false;

	public function __construct()
	{
		$this->setCpu('Intel Pentium Quad Core J2900 (2.41 GHz)');
		$this->setRam('4 Gb');
		$this->setVideo('Intel HD Graphics');
		$this->setMemory('HDD 1 Tb');
		$this->setComputerName('Lenovo E50-00');
	}

	public function identifyUser()
	{
		echo PHP_EOL.$this->getComputerName() . ': Identify by fingerprints' . PHP_EOL;
	}
}