<?php
class MacBook extends Computer
{
	const IS_DESKTOP = false;

	public function __construct()

	{
		$this->setCpu('Intel Core i5 (1.6 - 2.7 GHz)');
		$this->setRam('8 Gb');
		$this->setVideo('Intel HD Graphics 6000');
		$this->setMemory('SSD 256 Gb');
		$this->setComputerName('Apple MacBook Air 13"');
	}

	public function identifyUser()
	{
		echo PHP_EOL.$this->getComputerName() . ': Identify by Apple ID' . PHP_EOL;
	}
}
